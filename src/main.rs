use std::{io::stdout, process::exit};

use anyhow::Result;
use crossterm::{style::Stylize, terminal::size, tty::IsTty};
use scraper::{Html, Selector};
use whoami::lang;

mod cli;
mod io_functions;
mod selectors;

fn main() -> Result<()> {
    let main_array = selectors::details();
    let selector_list = main_array.iter().map(|(x, _, _, _)| *x).collect::<Vec<&str>>();
    let args = cli::build(&selector_list).get_matches();

    if args.get_flag("list") {
        selectors::print_list(&main_array);
    }
    if args.get_flag("clean") {
        match io_functions::clean_cache() {
            Ok(path) => {
                println!(
                    "{} The directory {} and its contents have been removed!",
                    "success:".green().bold(),
                    path.blue()
                );
                exit(0);
            }
            Err(e) => {
                eprintln!("{} {}", "error:".red().bold(), e);
                exit(1);
            }
        }
    }

    let use_cache = args.get_flag("cache");
    let query: Vec<&str> = if use_cache {
        vec![]
    } else {
        if let Some(query_str) = args.get_one::<String>("query") {
            query_str.split_whitespace().collect()
        } else {
            Vec::new()
        }
    };

    if query.len() == 1 && query[0] == "-" {
        println!(
            "{} The following required arguments were not provided:\n    {}\n\nUSAGE:\n    pooi <query>...\n\nFor more information try {}",
            "error:".red().bold(),
            "<query>...".green(),
            "--help".green()
        );
        exit(1);
    }

    let mut tty = stdout().is_tty();
    let tty_size = size().unwrap_or((0, 0));
    let w: usize = match tty_size.0 {
        0 if tty => panic!("main: can't determine terminal size"),
        0 => 0,
        1..=100 => tty_size.0.into(),
        _ => 100,
    };

    if args.get_flag("raw") {
        tty = false;
    }

    let quiet = if tty { args.get_flag("quiet") } else { true };

    let html = if use_cache {
        io_functions::cached_html()?
    } else {
        let lang = args
            .get_one::<String>("language")
            .map(|s| s.to_string())
            .unwrap_or_else(|| lang().next().unwrap_or("en-US".to_string()));
        io_functions::fetch(query.join(" "), lang)?
    };

    if args.get_flag("save") {
        if let Err(e) = io_functions::save_html(&query, &html) {
            eprintln!("{} {}\n", "error:".red().bold(), e);
        } else if tty {
            println!(
                "{}\n    {}\n",
                "HTML for the query has been saved to the following path:".dark_grey(),
                query.join(" ").blue()
            );
        }
    }

    let data = Html::parse_document(&html);
    let mut answers = vec![];
    for selector in &selector_list {
        let selector_str = match *selector {
            "corrections" => selectors::name_to_id("corrections"),
            _ => {
                let index = selector_list.iter().position(|&r| r == *selector).unwrap();
                main_array[index].3
            }
        };
        if data.select(&Selector::parse(selector_str).unwrap()).next().is_some() {
            if *selector == "holidays" {
                if let Some(element) = data.select(&Selector::parse("div.wDYxhc").unwrap()).nth(1) {
                    if element.value().attr("data-attrid") == Some("kc:/public_events:holidays_for_date") {
                        answers.push(*selector);
                    }
                }
            } else {
                answers.push(*selector);
            }
        }
    }

    match answers.len() {
        0 => {
            no_result(tty, w, &data, quiet, false)?;
            exit(1);
        }
        1 if answers[0] == "corrections" => {
            no_result(tty, w, &data, quiet, true)?;
            exit(1);
        }
        _ => {}
    }

    let mut corrected = false;
    if answers.last() == Some(&"corrections") {
        corrected = true;
        if !quiet {
            corrections(&data)?;
        }
        answers.pop();
    }

    let matches = answers.clone();
    if !args.get_flag("all") && answers.len() > 1 {
        let refined_query = if corrected {
            data.select(&Selector::parse(selectors::name_to_id("corrections")).unwrap())
                .next()
                .unwrap()
                .text()
                .collect::<Vec<&str>>()
                .join("")
        } else if use_cache {
            let title = data.select(&Selector::parse("title").unwrap())
                .next()
                .unwrap()
                .text()
                .collect::<Vec<&str>>()
                .join(" ");
            title.split_whitespace().take(title.split_whitespace().count() - 3).collect::<Vec<&str>>().join(" ")
        } else {
            query.join(" ")
        };
        answers = selectors::filter(answers, refined_query);
    }
    selectors::print_answer(&data, answers, &tty, w, &quiet, matches);

    if args.get_flag("urls") {
        print_urls(w, &data)?;
    }

    Ok(())
}

fn no_result(
    tty: bool,
    w: usize,
    data: &Html,
    quiet: bool,
    corrected: bool,
) -> Result<()> {
    if tty {
        if quiet {
            println!("{} Sorry about that!", "No result:".red().bold());
        } else {
            if corrected {
                corrections(data)?;
            }
            println!(
                "{} Perhaps one of these links might help?",
                "No result:".bold().red()
            );
            print_urls(w, data)?;
        }
    } else {
        eprintln!("No result!");
    }
    Ok(())
}

fn corrections(data: &Html) -> Result<()> {
    if let Some(correction_element) = data.select(&Selector::parse(selectors::name_to_id("corrections")).unwrap()).next() {
        let inner_html = correction_element.inner_html();
        let text_content = correction_element.text().collect::<Vec<&str>>().join("");

        let html_parts: Vec<&str> = inner_html.split_whitespace().collect();
        let text_parts: Vec<&str> = text_content.split_whitespace().collect();

        assert_eq!(html_parts.len(), text_parts.len());

        print!("{}", "I'll assume you meant this: ".dark_grey());
        for (html_part, text_part) in html_parts.iter().zip(text_parts.iter()) {
            if html_part == text_part {
                print!("{} ", text_part);
            } else {
                print!("{} ", text_part.bold().cyan());
            }
        }
        println!();
    }
    Ok(())
}

fn print_urls(w: usize, data: &Html) -> Result<()> {
    let url_selector = Selector::parse(selectors::name_to_id("url_block")).unwrap();
    let mut url_blocks = data.select(&url_selector);

    if url_blocks.next().is_some() {
        for block in url_blocks {
            let title_selector = Selector::parse(selectors::name_to_id("title")).unwrap();
            let title = block.select(&title_selector).next().unwrap().text().collect::<Vec<&str>>();

            let url_selector = Selector::parse(selectors::name_to_id("url")).unwrap();
            let url_element = block.select(&url_selector).next().unwrap();
            let href = url_element
                .first_child()
                .and_then(|element| element.value().as_element())
                .and_then(|element| element.attr("href"))
                .unwrap_or("No URL available");

            let desc_selector = Selector::parse(selectors::name_to_id("desc")).unwrap();
            let description = block.select(&desc_selector).next()
                .map_or("No description available, sorry!".to_string(), |desc| {
                    desc.text().collect::<Vec<&str>>().join("")
                });

            println!(
                "\n{}\n{}\n{}",
                title.join("").bold().blue(),
                href,
                format_desc(w, description).dark_grey()
            );
        }
    } else {
        println!("{}", "jk, there are no links!".dark_grey());
    }
    Ok(())
}

fn format_desc(length_max: usize, desc: String) -> String {
    if desc.len() < length_max {
        return desc;
    }

    let mut length = 0;
    let mut desc_build = vec![];
    let mut formatted_desc = vec![];

    let desc_words: Vec<&str> = desc.split_whitespace().collect();
    for word in &desc_words {
        if length + word.len() + 1 >= length_max {
            formatted_desc.push(desc_build.join(" "));
            desc_build.clear();
            length = word.len() + 1;
        } else {
            length += word.len() + 1;
        }
        desc_build.push(*word);
    }
    formatted_desc.push(desc_build.join(" "));
    formatted_desc.join("\n")
}
